#!/bin/sh

set -x
set -e

CHANGES_FILE=$1

sudo /usr/sbin/piuparts $CHANGES_FILE
autopkgtest $CHANGES_FILE -- qemu /var/lib/autopkgtest/sid.qemu
